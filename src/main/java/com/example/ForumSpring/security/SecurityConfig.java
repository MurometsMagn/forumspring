package com.example.ForumSpring.security;

import com.example.ForumSpring.services.UserService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final UserService userService;
    private final HandlerExceptionResolver resolver;

    public SecurityConfig(UserService userService,
                          @Qualifier("handlerExceptionResolver") HandlerExceptionResolver resolver) {
        this.userService = userService;
        this.resolver = resolver;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.anonymous()
                .and()
                .addFilterAt(new JWTFilter(userService, resolver), CsrfFilter.class)
                //.csrf().ignoringAntMatchers("/**")
                //.and()
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/users/login", "/users/register")
                .permitAll()
                .anyRequest()
                //.permitAll();
                .authenticated()
//                .and()
//                .httpBasic()
//                .authenticationEntryPoint((request, response, e) ->
//                        resolver.resolveException(request, response, null, e));
        ;
    }
}

//todo: hometask 16.12.21
/*
 *  протестировать авторизацию и исправить все возникшие проблемы
 *  1. /users/login открывается свободно
 *  2. при корректных логине и пароле /users/login возвращает jwt
 *  3. обращение к любому адресу кроме /users/login или /users/register возвращает ошибку 401
 *  4. указание заголовка authorization: bearer с полученной jwt - позволяет обратиться к указанному адресу
 */
