package com.example.ForumSpring.security;

import com.example.ForumSpring.exceptions.LoginFailedException;
import com.example.ForumSpring.exceptions.UserNotFoundException;
import com.example.ForumSpring.models.entities.User;
import com.example.ForumSpring.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;

@Slf4j
public class JWTFilter extends OncePerRequestFilter {
    private final UserService userService;
    private final HandlerExceptionResolver resolver;

    public JWTFilter(UserService userService, HandlerExceptionResolver resolver) {
        this.userService = userService;
        this.resolver = resolver;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        log.debug(request.getRequestURI());
        if (request.getRequestURI().startsWith("/users/login")) {
            log.debug("logging in");
            filterChain.doFilter(request, response);
            return;
        }
        String authorization = request.getHeader("Authorization");
        if (authorization == null) authorization = "";
        String[] authList = authorization.split(" ");
        if (authList.length != 2) {
            resolver.resolveException(request, response, null, new LoginFailedException("No JWT"));
            return;
        }
        if (!authList[0].toLowerCase(Locale.ROOT).equals("bearer")) {
            resolver.resolveException(request, response, null, new LoginFailedException("Invalid authorization type"));
            return;
        }
        String jwt = authList[1];
        //System.out.println("jwt = " + jwt);
        log.debug("jwt {}", jwt);
        long userId;
        try {
            userId = userService.parseJwt(jwt);
        } catch (LoginFailedException e) {
            log.debug(e.getMessage());
            resolver.resolveException(request, response, null, e);
            return;
        }
        User user;
        try {
            user = userService.getUserById(userId);
        } catch (UserNotFoundException e) {
            log.debug(e.getMessage());
            resolver.resolveException(request, response, null, e);
            return;
        }
        request.setAttribute(User.class.getCanonicalName(), user);
        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(user, null, null);
        auth.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        SecurityContext context = SecurityContextHolder.getContext();
        context.setAuthentication(auth);
        request.getSession(true).setAttribute("SPRING_SECURITY_CONTEXT", context);
        log.debug("toController");
        filterChain.doFilter(request, response);
    }
}
