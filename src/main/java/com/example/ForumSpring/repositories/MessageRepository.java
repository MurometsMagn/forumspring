package com.example.ForumSpring.repositories;

import com.example.ForumSpring.models.entities.Message;
import org.springframework.data.repository.CrudRepository;

public interface MessageRepository extends CrudRepository<Message, Long> {
}
