package com.example.ForumSpring.repositories;

import com.example.ForumSpring.models.entities.Topic;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface TopicRepository extends CrudRepository<Topic, Long> {
    Optional<Topic> findByName(String name);

    List<Topic> findByCreatorId(long creatorId);
}
