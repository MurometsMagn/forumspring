package com.example.ForumSpring.strategies;

import org.springframework.stereotype.Component;

@Component("two")
public class StrategyTwo implements ExampleStrategy{
    @Override
    public void doSomeThing(int param) {

    }
}
