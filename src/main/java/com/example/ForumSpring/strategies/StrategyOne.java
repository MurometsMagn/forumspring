package com.example.ForumSpring.strategies;

import org.springframework.stereotype.Component;

@Component("one") //"one" - это имя квалификатора
public class StrategyOne implements ExampleStrategy{
    @Override
    public void doSomeThing(int param) {

    }
}
