package com.example.ForumSpring.services;

import com.example.ForumSpring.exceptions.TopicAlreadyExistsException;
import com.example.ForumSpring.exceptions.TopicNotFoundException;
import com.example.ForumSpring.models.dto.TopicGetDTO;
import com.example.ForumSpring.models.dto.TopicWithMessagesDTO;
import com.example.ForumSpring.models.entities.Topic;
import com.example.ForumSpring.repositories.TopicRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TopicService {
    //private final UserRepository userRepo;
    private final TopicRepository topicRepo;

    public TopicService(TopicRepository topicRepo) {
        this.topicRepo = topicRepo;
    }


    public TopicGetDTO createTopic(String header, long userId) throws TopicAlreadyExistsException {
        Topic topic = new Topic();
        if (topicRepo.findByName(header).isPresent()) {
            throw new TopicAlreadyExistsException();
        } else {
            topic.setCreatorId(userId);
            topic.setName(header);
            topicRepo.save(topic);
            return TopicGetDTO.fromTopic(topic);
        }
    }

    public TopicWithMessagesDTO getTopic(long id) throws TopicNotFoundException {
        Optional<Topic> topic = topicRepo.findById(id);
        if (topic.isPresent()) {
            return TopicWithMessagesDTO.fromTopic(topic.get());
        }
        throw new TopicNotFoundException();
    }

    public List<TopicGetDTO> getAllTopics() {
        List<TopicGetDTO> topics = new ArrayList<>();
        for (var topic : topicRepo.findAll()) {
            topics.add(TopicGetDTO.fromTopic(topic));
        }
        return topics;
    }

    public List<TopicGetDTO> getTopicsOfUser(long authorId) {
        List<TopicGetDTO> topics = new ArrayList<>();
        for (var topic : topicRepo.findByCreatorId(authorId)) {
            topics.add(TopicGetDTO.fromTopic(topic));
        }
        return topics;
    }

    public TopicGetDTO getTopicByName(String header) throws TopicNotFoundException {
        Optional<Topic> topic = topicRepo.findByName(header);
        if (topic.isPresent()) {
            return TopicGetDTO.fromTopic(topic.get());
        }
        throw new TopicNotFoundException();
    }

    public void deleteTopic(long id) throws TopicNotFoundException {
        Optional<Topic> topic = topicRepo.findById(id);
        topic.ifPresent(topicRepo::delete); //!!!
        /*
        if (topic.isPresent()) {
            topicRepo.delete(topic.get());
        }
         */
        throw new TopicNotFoundException();
    }
}
