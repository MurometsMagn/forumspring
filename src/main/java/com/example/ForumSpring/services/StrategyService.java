package com.example.ForumSpring.services;

import antlr.collections.impl.LList;
import com.example.ForumSpring.strategies.ExampleStrategy;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

@Service
public class StrategyService {
    //два способа сбора коллекции при помощи DI
    private final List<ExampleStrategy> strategies; //может быть Set
    private final Map<String, ExampleStrategy> strategyMap; //квалификатор компонента -это ключ, а экз-р - это значение
    //String string = `dkfjkd`;
    public StrategyService(List<ExampleStrategy> strategies, Map<String, ExampleStrategy> strategyMap) {
        this.strategies = strategies;
        this.strategyMap = strategyMap;
    }
}
