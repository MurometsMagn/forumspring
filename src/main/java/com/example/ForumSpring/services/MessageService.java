package com.example.ForumSpring.services;

import com.example.ForumSpring.models.dto.MessageGetDTO;
import org.springframework.stereotype.Service;
import com.example.ForumSpring.models.entities.Message;
import com.example.ForumSpring.repositories.MessageRepository;

import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class MessageService {
    private final MessageRepository messageRepo;
//    private final UserRepository userRepo;
//    private final TopicRepository topicRepo;

    public MessageService(MessageRepository messageRepo) {
        this.messageRepo = messageRepo;
    }

    //почему нотФаунд -требует проброса, а НоуСачЭлемент - нет?
    public MessageGetDTO sendMessage(long topicId, long userId, String messageText) {
        Message message = new Message();
        //Optional<User> user = userRepo.findById(userId);
        //Optional<Topic> topic = topicRepo.findById(topicId);

        message.setAuthorId(userId);
        message.setTopicId(topicId);
        message.setText(messageText);
        messageRepo.save(message);
        return MessageGetDTO.fromMessage(message);
    }

    public MessageGetDTO receiveMessage(long id) {
        Optional<Message> message = messageRepo.findById(id);
        if (message.isPresent()) {
            return MessageGetDTO.fromMessage(message.get());
        }
        throw new NoSuchElementException("Сообщение не найдено");
    }
}
