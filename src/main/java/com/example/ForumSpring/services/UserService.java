package com.example.ForumSpring.services;

import com.example.ForumSpring.exceptions.ElementNotFoundException;
import com.example.ForumSpring.exceptions.LoginFailedException;
import com.example.ForumSpring.exceptions.UserAlreadyExistsException;
import com.example.ForumSpring.exceptions.UserNotFoundException;
import com.example.ForumSpring.models.dto.LoginResponseDTO;
import com.example.ForumSpring.models.dto.UserGetDTO;
import com.example.ForumSpring.models.dto.UserPostDTO;
import com.example.ForumSpring.models.entities.User;
import com.example.ForumSpring.repositories.UserRepository;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;


@Service
@Slf4j
public class UserService implements UserDetailsService {
    private static final String signingKey = "MTIzNDU="; //12345
    private final UserRepository userRepo;
    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    public UserService(UserRepository userRepo) {
        this.userRepo = userRepo;
    }

    public LoginResponseDTO login(String login, String password) throws LoginFailedException {
        Optional<User> userOptional= userRepo.findByLogin(login);
        if (userOptional.isEmpty()) {
            throw new LoginFailedException("Нет такого пользователя"); //неверный логин
        }
        User user = userOptional.get();
        if (!passwordEncoder.matches(password, user.getPassword())) {
            throw new LoginFailedException("Нет такого пользователя"); //неверный пароль
        }
        Map<String, Object> claims = new HashMap<>();
        claims.put("user_id", user.getId());
        claims.put("user_name", user.getLogin());
        String token = Jwts.builder()
                .setClaims(claims)
                .setSubject("myForum") //кто мы есть
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60))
                .signWith(SignatureAlgorithm.HS512, signingKey) //12345 //
                .compact();
        log.debug(token);
        return new LoginResponseDTO(token);
    }

    public long parseJwt(String jwt) throws LoginFailedException{
        try {
            return Jwts.parser()
                    .requireSubject("myForum")
                    .setSigningKey(signingKey)
                    .parseClaimsJws(jwt)
                    .getBody()
                    .get("user_id", Long.class);
        } catch (JwtException jwtException) {
            log.debug(jwtException.getMessage());
            throw new LoginFailedException("Invalid jwt");
        }
    }

    public void registration(UserPostDTO userPostDTO) throws UserAlreadyExistsException {
        //User user = new User(0, userPostDTO.getLogin(), false, null, null);
        if (userRepo.findByLogin(userPostDTO.getLogin()).isPresent()) {
            throw new UserAlreadyExistsException();
        }
        //userRepo.save(userPostDTO);
    }

    public UserGetDTO getOne(long id) throws ElementNotFoundException {
        Optional<User> user = userRepo.findById(id);
        if (user.isPresent()) {
            return UserGetDTO.fromUser(user.get());
        }
        throw new UserNotFoundException();
    }

    public User getUserById(long id) throws UserNotFoundException {
        return userRepo.findById(id).orElseThrow(UserNotFoundException::new);
    }

    public List<UserGetDTO> getAll() {
        List<UserGetDTO> result = new ArrayList<>();
        for (var user : userRepo.findAll()) {
            result.add((UserGetDTO.fromUser(user)));
        }
        return result;
    }

    public void delete(long id) throws UserNotFoundException {
        if (userRepo.findById(id).isPresent()) {
            userRepo.deleteById(id);
        }
        throw new UserNotFoundException();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepo.findByLogin(username);
        if (user.isEmpty()) {
            throw new UsernameNotFoundException("Нет такого пользователя");
        }
        return user.get();
    }
}

