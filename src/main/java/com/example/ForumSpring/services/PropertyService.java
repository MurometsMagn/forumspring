package com.example.ForumSpring.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class PropertyService {
    @Value("${forumspring.numworkers}") //$ - означает что сюда будет подставлена переменная из application.properties
    private int numWorkers;
}
