package com.example.ForumSpring.controllers;

import com.example.ForumSpring.exceptions.TopicAlreadyExistsException;
import com.example.ForumSpring.exceptions.TopicNotFoundException;
import com.example.ForumSpring.models.dto.*;
import com.example.ForumSpring.models.entities.User;
import com.example.ForumSpring.services.TopicService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/topics")
public class TopicController {
    private final TopicService topicService;

    public TopicController(TopicService topicService) {
        this.topicService = topicService;
    }

    @PostMapping
    public TopicGetDTO createTopic(@RequestBody TopicPostDTO topic, HttpServletRequest request) throws TopicAlreadyExistsException {
        User user = (User) request.getAttribute(User.class.getCanonicalName());
        return topicService.createTopic(topic.getHeader(), user.getId());
    }

    @GetMapping
    public List<TopicGetDTO> getAllTopics() {
        log.debug("getAllTopics");
//        List<TopicGetDTO> list = new ArrayList<>();
//        list.add(new TopicGetDTO(1, new AuthorGetDTO(1, "firstUser"), "hello", ZonedDateTime.now()));
//        list.add(new TopicGetDTO(2, new AuthorGetDTO(2, "secondUser"), "secondTopic", ZonedDateTime.now()));
//        return list;
        return topicService.getAllTopics();
    }

    @GetMapping("{id}")
    public TopicWithMessagesDTO getTopic(@PathVariable long id) {
        try {
            return topicService.getTopic(id);
        } catch (TopicNotFoundException e) {
            e.printStackTrace();
            return null;
        }



//        return new TopicWithMessagesDTO("Header", List.of(
//                new MessageGetDTO(1, new AuthorGetDTO(11, "firstAuthor"), 1, "text Of Message", ZonedDateTime.now()),
//                new MessageGetDTO(2, new AuthorGetDTO(12, "secondAuthor"), 1, "second message text", ZonedDateTime.now())
//        ));
    }
}

//todo: homeTask21.12.21
/*
 отладить получение данных GET/topic
 */

//todo: homeTask29.03.22
/*
 помимо id автора возвращать еще его имя
 */
