package com.example.ForumSpring.controllers;

import com.example.ForumSpring.models.dto.MessageGetDTO;
import com.example.ForumSpring.models.dto.MessagePostDTO;
import com.example.ForumSpring.models.entities.User;
import com.example.ForumSpring.services.MessageService;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/messages")
public class MessageController {
    private final MessageService messageService;

    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @PostMapping
    public MessageGetDTO sendMessage(@RequestBody MessagePostDTO message, HttpServletRequest request) {
        User user = (User) request.getAttribute(User.class.getCanonicalName());
        return messageService.sendMessage(message.getTopicId(), user.getId(), message.getText());
    }

    @GetMapping
    public List<MessageGetDTO> receiveAllMessages() {

        return null;
    }

    @GetMapping("{topic_id}")
    public List<MessageGetDTO> receiveAllMessagesOfTopic(@PathVariable long topicId) {
        return null;
    }
}

//todo: hometask 23.12.21
/*
 1. исправить ошибку с Hibernate (по примеру с занаятием: настройка в aplication.propities)
 1. добавить topic при помощи postman
 3. добавить message при помощи postman в topic, созданый в п.2
 4. реализовать прочие методы контроллеров
 */
