package com.example.ForumSpring.controllers;

import com.example.ForumSpring.exceptions.ElementNotFoundException;
import com.example.ForumSpring.exceptions.LoginFailedException;
import com.example.ForumSpring.exceptions.UserAlreadyExistsException;
import com.example.ForumSpring.exceptions.UserNotFoundException;
import com.example.ForumSpring.models.dto.LoginDTO;
import com.example.ForumSpring.models.dto.LoginResponseDTO;
import com.example.ForumSpring.models.dto.UserGetDTO;
import com.example.ForumSpring.models.dto.UserPostDTO;
import com.example.ForumSpring.services.UserService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    /*
     1.получить пользователя из репозитория по логину
     2.сверить хэш пароля с паролем пользователя
     3.если они совпадают - сгенерировать jwt (json web token) и отдать его пользователю
     */
    @PostMapping("/login")
    public LoginResponseDTO login(@RequestBody LoginDTO body) throws LoginFailedException {
        return userService.login(body.getLogin(), body.getPassword());
    }

    @GetMapping
    public List<UserGetDTO> getAll() {
        return userService.getAll();
    }

    @GetMapping("{id}")
    public UserGetDTO getById(@PathVariable long id) throws ElementNotFoundException {
            return userService.getOne(id);
    }

    @PostMapping("/register")
    public void registration(@RequestBody UserPostDTO body) throws UserAlreadyExistsException {
            userService.registration(body);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable Long id) throws UserNotFoundException {
            userService.delete(id);
    }
}
