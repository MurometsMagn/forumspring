package com.example.ForumSpring.exceptions;

public class TopicAlreadyExistsException extends ElementAlreadyExistsException {

    public TopicAlreadyExistsException() {
        super("Teма с таким именем уже существует");
    }

    public TopicAlreadyExistsException(String text) {
        super(text);
    }
}
