package com.example.ForumSpring.exceptions;

import lombok.experimental.StandardException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

//@StandardException
@ResponseStatus(code = HttpStatus.NOT_FOUND) //404
public class TopicNotFoundException extends ElementNotFoundException {
    public TopicNotFoundException() {
        super("Тема не найдена");
    }

    public TopicNotFoundException(String text) {
        super(text);
    }
}
