package com.example.ForumSpring.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND) //404
public class UserNotFoundException extends ElementNotFoundException {
    public UserNotFoundException() {
        super("Пользователь не найден");
    }

    public UserNotFoundException(String text) {
        super(text);
    }
}
