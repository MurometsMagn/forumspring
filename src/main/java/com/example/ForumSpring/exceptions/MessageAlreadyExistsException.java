package com.example.ForumSpring.exceptions;

public class MessageAlreadyExistsException extends ElementAlreadyExistsException{

    public MessageAlreadyExistsException() {
        super("Сообщение с таким именем уже существует");
    }

    public MessageAlreadyExistsException(String text) {
        super(text);
    }
}
