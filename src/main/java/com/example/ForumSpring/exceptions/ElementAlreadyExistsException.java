package com.example.ForumSpring.exceptions;

import lombok.experimental.StandardException;

@StandardException
public class ElementAlreadyExistsException extends Exception{

    public ElementAlreadyExistsException(String text) {
        super(text);
    }
}
