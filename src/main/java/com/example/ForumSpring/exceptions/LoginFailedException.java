package com.example.ForumSpring.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.UNAUTHORIZED) //401
public class LoginFailedException extends Exception {
    public LoginFailedException(String message) {
        super(message);
    }
}
