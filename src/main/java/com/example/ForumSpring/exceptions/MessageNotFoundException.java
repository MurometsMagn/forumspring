package com.example.ForumSpring.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND) //404
public class MessageNotFoundException extends ElementNotFoundException {

    public MessageNotFoundException() {
        super("Cообщение не найдено");
    }

    public MessageNotFoundException(String text) {
        super(text);
    }
}
