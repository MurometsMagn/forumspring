package com.example.ForumSpring.exceptions;

public class UserAlreadyExistsException extends ElementAlreadyExistsException {
    public UserAlreadyExistsException() {
        super("Пользователь с таким именем уже существует");
    }

    public UserAlreadyExistsException(String text) {
        super(text);
    }
}
