package com.example.ForumSpring.exceptions;

import lombok.experimental.StandardException;

@StandardException  //генерация всех требуемых конструкторов Exception

public class ElementNotFoundException extends Exception {

    public ElementNotFoundException(String text) {
        super(text);
    }
}
