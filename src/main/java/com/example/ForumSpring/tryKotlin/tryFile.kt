package com.example.ForumSpring.tryKotlin

fun main(){
    val string = """\n""" // тройные кавычки исп для обозначения многострочных строк, но из-за того, что они не преобразуют эскейп-последовательнгости (напр \n), могут исп-ся и как "сырые" строки (Raw-String)
    println("\"$string\"") // "\n"
}