package com.example.ForumSpring.models.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TopicPostDTO extends TopicBaseDTO{

    public TopicPostDTO(String header) {
        super(header);
    }
}
