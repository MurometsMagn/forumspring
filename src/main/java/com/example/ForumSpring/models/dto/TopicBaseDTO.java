package com.example.ForumSpring.models.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TopicBaseDTO {
    private String header;

    public TopicBaseDTO(String header) {
        this.header = header;
    }
}
