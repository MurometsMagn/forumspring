package com.example.ForumSpring.models.dto;

import com.example.ForumSpring.models.entities.Topic;
import lombok.Getter;
import lombok.Setter;

import java.time.ZonedDateTime;

@Getter
@Setter
public class TopicGetDTO  extends TopicBaseDTO{
    private long id;
    private AuthorGetDTO author;
    private ZonedDateTime createdDateTime;

    public TopicGetDTO(long id, AuthorGetDTO author, String header, ZonedDateTime createdDateTime) {
        super(header);
        this.id = id;
        this.author = author;
        this.createdDateTime = createdDateTime;
    }

    public static TopicGetDTO fromTopic(Topic topic) {
        return new TopicGetDTO(topic.getId(),
                new AuthorGetDTO(topic.getCreator().getId(), topic.getCreator().getUsername()),
                topic.getName(), topic.getCreateDateTime());
    }
}
