package com.example.ForumSpring.models.dto;

import com.example.ForumSpring.models.entities.Topic;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
public class TopicWithMessagesDTO {
    private String header;
    private List<MessageGetDTO> messages;

    public static TopicWithMessagesDTO fromTopic(Topic topic) {
        return new TopicWithMessagesDTO(
                topic.getName(),
                topic.getMessages().stream()
                        .map(MessageGetDTO::fromMessage)
                        .collect(Collectors.toList())
        );
    }
}
