package com.example.ForumSpring.models.dto;

import com.example.ForumSpring.models.entities.Message;
import lombok.Getter;
import lombok.Setter;

import java.time.ZonedDateTime;

@Getter
@Setter
public class MessageGetDTO extends MessageBaseDTO {
    private long id;
    private AuthorGetDTO author;
    private ZonedDateTime createDateTime;

    public MessageGetDTO(long id, AuthorGetDTO author, long topicId, String text, ZonedDateTime createDateTime) {
        super(text, topicId);
        this.id = id;
        this.author = author;
        this.createDateTime = createDateTime;
    }

    public static MessageGetDTO fromMessage(Message message) {
        return new MessageGetDTO(message.getId(),
                new AuthorGetDTO(message.getAuthor().getId(),
                        message.getAuthor().getUsername()),
                message.getTopic().getId(), message.getText(), message.getCreateDateTime());
    }

}