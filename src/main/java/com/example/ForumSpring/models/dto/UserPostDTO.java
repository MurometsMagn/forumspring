package com.example.ForumSpring.models.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserPostDTO extends UserBaseDTO{
    private String password;

    public UserPostDTO(String login, String password) {
        super(login);
        this.password = password;
    }
}
