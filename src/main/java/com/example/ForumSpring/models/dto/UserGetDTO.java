package com.example.ForumSpring.models.dto;

import com.example.ForumSpring.models.entities.User;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserGetDTO extends UserBaseDTO{
    private final long id;

    public UserGetDTO(long id, String login) {
        super(login);
        this.id = id;
    }

    public static UserGetDTO fromUser(User user) {
        return new UserGetDTO(user.getId(), user.getLogin());
    }
}
