package com.example.ForumSpring.models.dto;

import lombok.Getter;
import lombok.Setter;

//в отличие от UserPostDTO должен содержать ТОЛЬКО логин и пароль
@Getter
@Setter
public class LoginDTO {
    private String login;
    private String password;

    public LoginDTO(String login, String password) {
        this.login = login;
        this.password = password;
    }
}
