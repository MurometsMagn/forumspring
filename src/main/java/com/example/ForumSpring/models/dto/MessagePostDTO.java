package com.example.ForumSpring.models.dto;

public class MessagePostDTO extends MessageBaseDTO{

    public MessagePostDTO(String text, long topicId) {
        super(text, topicId);
    }
}
