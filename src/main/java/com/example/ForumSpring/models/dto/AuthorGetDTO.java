package com.example.ForumSpring.models.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthorGetDTO { //усеченная версия UserGetDTO
    private long id;
    private String name;
}
