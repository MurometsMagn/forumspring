package com.example.ForumSpring.models.dto;

import lombok.Getter;
import lombok.Setter;

//на случай добавления полей в юзера: реалНэйм и т.п.
@Getter
@Setter
public class UserBaseDTO {
    private String login;

    public UserBaseDTO(String login) {
        this.login = login;
    }
}
