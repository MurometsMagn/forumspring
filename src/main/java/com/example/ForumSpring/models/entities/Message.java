package com.example.ForumSpring.models.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "messages")
public class Message {
    @Id
    private long id;
    private String text;
    @Column(name = "author_id", insertable = false, updatable = false)
    private long authorId;
    @Column(name = "topic_id", insertable = false, updatable = false)
    private long topicId;
    private ZonedDateTime createDateTime;

    @ManyToOne
    @JoinColumn(name = "author_id")
    private User author;

    @ManyToOne
    @JoinColumn(name = "topic_id")
    private Topic topic;
}
