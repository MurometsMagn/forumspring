package com.example.ForumSpring.models.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "topics")
public class Topic {
    @Id
    private long id;
    private String name;
    @Column(name = "creator_id", insertable = false, updatable = false)
    private long creatorId;

    @ManyToOne
    @JoinColumn(name = "creator_id")
    private User creator;

    @OneToMany(mappedBy = "topic")
    private List<Message> messages;

    public ZonedDateTime getCreateDateTime() {
        return messages.isEmpty()
                ? ZonedDateTime.now()
                : messages.get(0).getCreateDateTime();
    }
}

// TODO: 7.06.22  homeTask
/*
 1. Добавить в БД тестовый месседж и протестировать его работоспособность
 2. Убедиться в том, что в шаблонах на клиенте отображаются данные из БД, а не заглушки
 */
